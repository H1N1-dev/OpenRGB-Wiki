Yeelight smart lights can be controlled by OpenRGB.  These bulbs must be set up and paired first using the official Yeelight app, but after they are connected to your network you can add your Yeelight lights to the OpenRGB configuration file.

An example Yeelight configuration entry:

```
    "YeelightDevices": {
        "devices": [
            {
                "ip": "192.168.4.11",
                "music_mode": true
            }
        ]
    }
```

* `ip` - IP address of Yeelight light.  You can find this in your WiFi router's administration page.
* `music_mode` - Boolean value.  Set `true` to use music mode, which enables Direct mode for effect engine use.  Default `false`.