The ASUS ROG Strix Flare keyboard uses USB HID with USB ID 0B05:1875.

LED map

| LED ID | Key           |
| ------ | ------------- |
| 0      | Escape        |
| 1      | `             |
| 2      | Tab           |
| 3      | Caps Lock     |
| 4      | Left Shift    |
| 5      | Left Control  |
| 6      |               |
| 7      |               |
| 8      |               |
| 9      |               |
| 10     |               |
| 11     |               |
| 12     |               |
| 13     | Left Windows  |
| 14     |               |
| 15     |               |
| 16     |               |
| 17     | 1             |
| 18     | Q             |
| 19     | A             |
| 20     | Z             |
| 21     | Left Alt      |
| 22     |               |
| 23     |               |
| 24     | F1            |
| 25     | 2             |
| 26     | W             |
| 27     | S             |
| 28     | X             |
| 29     |               |
| 30     |               |
| 31     |               |
| 32     | F2            |
| 33     | 3             |
| 34     | E             |
| 35     | D             |
| 36     | C             |
| 37     |               |
| 38     |               |
| 39     |               |
| 40     | F3            |
| 41     | 4             |
| 42     | R             |
| 43     | F             |
| 44     | V             |
| 45     |               |
| 46     |               |
| 47     |               |
| 48     | F4            |
| 49     | 5             |
| 50     | T             |
| 51     | G             |
| 52     | B             |
| 53     | Space         |
| 54     |               |
| 55     |               |
| 56     |               |
| 57     | 6             |
| 58     | Y             |
| 59     | H             |
| 60     | N             |
| 61     |               |
| 62     |               |
| 63     |               |
| 64     | F5            |
| 65     | 7             |
| 66     | U             |
| 67     | J             |
| 68     | M             |
| 69     |               |
| 70     |               |
| 71     |               |
| 72     | F6            |
| 73     | 8             |
| 74     | I             |
| 75     | K             |
| 76     | <             |
| 77     | Right Alt     |
| 78     |               |
| 79     |               |
| 80     | F7            |
| 81     | 9             |
| 82     | O             |
| 83     | L             |
| 84     | >             |
| 85     |               |
| 86     |               |
| 87     |               |
| 88     | F8            |
| 89     | 0             |
| 90     | P             |
| 91     | ;             |
| 92     | ?             |
| 93     | Fn            |
| 94     |               |
| 95     |               |
| 96     | F9            |
| 97     | -             |
| 98     | [             |
| 99     | '             |
| 100    |               |
| 101    | Menu          |
| 102    |               |
| 103    |               |
| 104    | F10           |
| 105    | =             |
| 106    | ]             |
| 107    |               |
| 108    |               |
| 109    |               |
| 110    |               |
| 111    |               |
| 112    | F11           |
| 113    |               |
| 114    |               |
| 115    |               |
| 116    |               |
| 117    |               |
| 118    |               |
| 119    |               |
| 120    | F12           |
| 121    | Backspace     |
| 122    | \ (ANSI)      |
| 123    | Enter         |
| 124    | Right Shift   |
| 125    | Right Control |
| 126    |               |
| 127    |               |
| 128    | Print Screen  |
| 129    | Insert        |
| 130    | Delete        |
| 131    |               |
| 132    |               |
| 133    | Left Arrow    |
| 134    |               |
| 135    |               |
| 136    | Scroll Lock   |
| 137    | Home          |
| 138    | End           |
| 139    |               |
| 140    | Up Arrow      |
| 141    | Down Arrow    |
| 142    |               |
| 143    |                  |
| 144    | Pause/Break      |
| 145    | Page Up          |
| 146    | Page Down        |
| 147    |                  |
| 148    |                  |
| 149    | Right Arrow      |
| 150    |                  |
| 151    |                  |
| 152    |                  |
| 153    | Num Lock         |
| 154    | Number Pad 7     |
| 155    | Number Pad 4     |
| 156    | Number Pad 1     |
| 157    | Number Pad 0     |
| 158    |                  |
| 159    |                  |
| 160    |                  |
| 161    | Number Pad /     |
| 162    | Number Pad 8     |
| 163    | Number Pad 5     |
| 164    | Number Pad 2     |
| 165    |                  |
| 166    |                  |
| 167    |                  |
| 168    |                  |
| 169    | Number Pad *     |
| 170    | Number Pad 9     |
| 171    | Number Pad 6     |
| 172    | Number Pad 3     |
| 173    | Number Pad .     |
| 174    |                  |
| 175    |                  |
| 176    |                  |
| 177    | Number Pad -     |
| 178    | Number Pad +     |
| 179    |                  |
| 180    | Number Pad Enter |
| 181    |                  |
| 182    |                  |
| 183    |                  |
| 184    | Logo             |
| 185    | Left Underglow   |
| 186    | Right Underglow  |