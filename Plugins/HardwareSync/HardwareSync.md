# How to use HardwareSync Plugin
## Quick presentation
**HardwareSync** is a plugin that allows you to assign a hardware measure to your devices.

For example, you can display your CPU temp to your fans, or your ram usage on your ramsticks.

**Windows users:** For the plugin to work, first download [lhwm-wrapper.dll](https://gitlab.com/OpenRGBDevelopers/OpenRGBHardwareSyncPlugin/-/raw/master/dependencies/lhwm-cpp-wrapper/x64/Release/lhwm-wrapper.dll) and place it in the OpenRGB root folder, then restart OpenRGB.
## How to use it?
[<img src="HWS_tuto.png" height="384">](HWS_tuto.png)

1. **Create a new measure**

    Cick on **new measure** on the top left corner. A new tab will appear.
2. **Assign the device you want to monitor by selecting it in the hardware list**

    In this menu you can choose any of your devices monitored by **lhwm-wrapper.dll**
3. **Choose the wanted measure**

    For example if you wanted to monitor the GPU usage, GPU core/hotspot Temp, Power consuption, VRAM usage then select it from this drop down combo box.
4. **Create your desired colour pattern based on values**

    If you want your device to display blue when your measure is at 40, and fade to red as the measure increases, set red to a higher value
5. **Apply your measure to your desired device**

    You can apply it to one or more devices, also on visual maps.

    Percentage fill will light your device based on the metric's value.

    *Example: GPU temp set to min 50 max 100, reporting as 75: half of your device will be enlightened*
6. **Start your measure**

    Click start to have your settings apply to the selected devices.
7. **Save your profile**

    You can tick **autostart** before saving, so your mesure will start automaticly with OpenRGB.<br/>
Now you can create a new measure, and apply it to another device if you want.

*If the device flickers because your hardware measure oscillates between two values, you may want to adjust the **refresh interval** to a higher value*
